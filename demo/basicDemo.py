# -*- coding: utf-8 -*-
''' 
	Title: basicDemo.py

	Description: 
		This is a basic demo to run on the large Tactip tactile fingertip sensor. This algorithm is 
		based on the work of;
			'Roke, C., Melhuish, C., & Pipe, T. (2011). Deformation-based tactile 
		feedback using a biologically-inspired sensor and a modified display. Towards Autonomous …, 
		114–124. Retrieved from http://link.springer.com/chapter/10.1007/978-3-642-23232-9_11'

		In short the algorithm partitions the camera image in to a grid array where each cell's average
		light value is measured. This value is affected by sensor deformation, such that objects 
		pressed in to the skin separate the internal white papillae pins creating larger regions of 
		empty black space.  

	Prerequisite: 
		Python 2.7 32bit
		OpenCV with python bindings

		make sure PythonPath is defined before running
		
	To install everything:
		Install python 32-bit direct from https://www.python.org/download/releases/2.7/ 
		Install pip http://pip.readthedocs.org/en/latest/installing.html
		Install numpy (download if pip doesnt work) http://heanet.dl.sourceforge.net/project/numpy/NumPy/1.7.1/numpy-1.7.1-win32-superpack-python2.7.exe
		Install matplotlib which requires python-dateutil and pyparsing (pip)
		Download and install opencv as http://docs.opencv.org/trunk/doc/py_tutorials/py_setup/py_setup_in_windows/py_setup_in_windows.html
		Install pyOSC https://trac.v2.nl/wiki/pyOSC

	To run: 
		on command line type "python basicDemo.py"

		The resolution of the grid can be changed by altering the gridSize object in the code below.

	License:
	    This file is part of Tactip demonstration software.

	    Tactip demonstration software is free software: you can redistribute it and/or modify
	    it under the terms of the GNU General Public License as published by
	    the Free Software Foundation, either version 3 of the License, or
	    (at your option) any later version.

	    Tactip demonstration software is distributed in the hope that it will be useful,
	    but WITHOUT ANY WARRANTY; without even the implied warranty of
	    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	    GNU General Public License for more details.

	    You should have received a copy of the GNU General Public License
	    along with Tactip demonstration software.  If not, see <http://www.gnu.org/licenses/>.

	    Copyright 2015 Bristol Robotics Laboratory

	Change history:

	|Date: 13/02/2015
	|Description: Demo has been tidied up for distribution.
	|Author: B Winstone, Bristol Robotics Laboratory, Benjamin.Winstone@brl.ac.uk.
	|Status: Currently works in windows 7, mac os and linux.


'''

import numpy
import cv2
import colorsys
import core.tactipCV as t
import socket 
import OSC

cv2.namedWindow("Tactip basic demo")
cam = t.initCamera( address=0)
rval, frame = cam.read()

# define region of interest in camera image
xstart = 0
ystart = 0
width = 400 # width of roi
height = 400 # height of roi


gridSizes = ((1,1),(2,2),(3,3),(4,4),(8,8))
gridSize = gridSizes[3]

element = cv2.getStructuringElement(cv2.MORPH_CROSS,(3,3))
lightValues = [[0 for i in range(gridSize[1])] for j in range(gridSize[0])]

send_address = '127.0.0.1', 8000 #internal OSC route
c = OSC.OSCClient()
c.connect(send_address)

print 'Press ''Q'' to quit.'

while True:

	if frame is not None:   
     
		cropped = t.cropFrame( frame, (xstart,ystart), (xstart+width,ystart+height) )
		gray = cv2.cvtColor(cropped, cv2.COLOR_BGR2GRAY) # create image to read grayscale value
		cropped = cv2.cvtColor(gray, cv2.COLOR_GRAY2BGR) # create grayscale, but colour image to draw on to

		msg = OSC.OSCMessage()
		msg.setAddress('/tactipReadings')

		# Itterate through n*n cell grid
		for y in xrange(gridSize[1]):
			for x in xrange(gridSize[0]):

				# calculate average light value inside cell region
				lightValues[x][y] = t.aveCellGray(gray, (x,y), size=width/gridSize[0] )
				
				# get pixel value
				reading = lightValues[x][y]

				print reading
				msg.append (reading)

				# convert light value to hsv
				r,g,b = colorsys.hsv_to_rgb(0.5,1,reading/255.0)
				r = int(r*255)
				g = int(g*255)
				b = int(b*255)

				# draw square for gui
				t.drawCell(cropped, (x,y), size=width/gridSize[0])
				t.fillCell(cropped, (x,y), (r,g,b), size=width/gridSize[0], border=10)

		c.send(msg)
		cv2.imshow("preview", cropped)

	rval, frame = cam.read()

	if cv2.waitKey(1) & 0xFF == ord('q'):
		break